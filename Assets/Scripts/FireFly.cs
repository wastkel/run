﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireFly : MonoBehaviour
{
    private GameObject pers;

	// Use this for initialization
	void Start ()
    {
        
	}
	
	// Update is called once per frame
	void Update ()
    {
        pers = GameObject.FindGameObjectWithTag("Player");
        gameObject.transform.position = Vector3.Lerp(gameObject.transform.position, pers.transform.position + new Vector3(2,2,-2), 5f * Time.deltaTime);
    }
}
