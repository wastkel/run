﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwipeTrail : MonoBehaviour
{

    public GameObject trailPrefab;
    GameObject thisTrail;
    Vector3 startPos;
    Plane objPlane;

	// Use this for initialization
	void Start ()
    {
        
        objPlane = new Plane(Camera.main.transform.forward * -1, this.transform.position);
	}

    // Update is called once per frame
    void Update()
    {
        if ((Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began) || Input.GetMouseButtonDown(0))
        {

            thisTrail = Instantiate(trailPrefab, transform.position, Quaternion.identity);
            Ray mRay = Camera.main.ScreenPointToRay(Input.mousePosition);
            float rayDistance;
            
            if (objPlane.Raycast(mRay, out rayDistance))
                startPos = mRay.GetPoint(rayDistance);
        }
        else if (((Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved) || Input.GetMouseButton(0)))
        {
            Ray mRay = Camera.main.ScreenPointToRay(Input.mousePosition);
            float rayDistance;
            thisTrail.AddComponent<PolygonCollider2D>();
            if (objPlane.Raycast(mRay, out rayDistance))
            {
                thisTrail.transform.position = mRay.GetPoint(rayDistance);
                
            }
        }
        else if ((Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended) || Input.GetMouseButtonUp(0))
        {
            
            if (Vector3.Distance(thisTrail.transform.position, startPos) < 0.1)
                Destroy(thisTrail);
        }
    }
    //    private void addColliderToLine()
    //{
    //    BoxCollider col = new GameObject("Collider").AddComponent<BoxCollider>();
    //    col.transform.parent = line.transform; // Collider is added as child object of line
    //    float lineLength = Vector3.Distance(startPos, endPos); // length of line
    //    col.size = new Vector3(lineLength, 0.1f, 1f); // size of collider is set where X is length of line, Y is width of line, Z will be set as per requirement
    //    Vector3 midPoint = (startPos + endPos) / 2;
    //    col.transform.position = midPoint; // setting position of collider object
    //    // Following lines calculate the angle between startPos and endPos
    //    float angle = (Mathf.Abs(startPos.y - endPos.y) / Mathf.Abs(startPos.x - endPos.x));
    //    if ((startPos.y < endPos.y && startPos.x > endPos.x) || (endPos.y < startPos.y && endPos.x > startPos.x))
    //    {
    //        angle *= -1;
    //    }
    //    angle = Mathf.Rad2Deg * Mathf.Atan(angle);
    //    col.transform.Rotate(0, 0, angle);
    //}
}
