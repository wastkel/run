﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;
using System.Text;

public class LanguageManager : MonoBehaviour
{
    private string json;
    public lang lng = new lang();
    [SerializeField] Text [] textFieldArray;

    private void Awake()
    {
        PlayerPrefs.DeleteKey("Language");
        if (!PlayerPrefs.HasKey("Language"))
        {
            if (Application.systemLanguage == SystemLanguage.Russian || Application.systemLanguage == SystemLanguage.Ukrainian || Application.systemLanguage == SystemLanguage.Belarusian)
            {
                PlayerPrefs.SetString("Language", "ru_RU");
            }
            else
                PlayerPrefs.SetString("Language", "en_US");
        }
        LangLoad();
    }

    private void Start()
    {
        for (int i = 0; i < textFieldArray.Length; i++)
        {
            textFieldArray[i].text = lng.language[i];
        }
    }

    void LangLoad()
    {

#if UNITY_EDITOR
        string filePath = Path.Combine(Application.streamingAssetsPath, PlayerPrefs.GetString("Language") + ".json");

#elif UNITY_IOS
        string filePath = Path.Combine (Application.streamingAssetsPath + "/Raw", PlayerPrefs.GetString("Language") + ".json");

#elif UNITY_ANDROID
        string filePath = Path.Combine ("jar:file://" + Application.dataPath + "!/assets/", PlayerPrefs.GetString("Language") + ".json");

#endif

#if UNITY_EDITOR || UNITY_IOS
        json = File.ReadAllText(filePath);

#elif UNITY_ANDROID
        WWW reader = new WWW (filePath);
        while (!reader.isDone) {
        }
        json = reader.text;
#endif
        lng = JsonUtility.FromJson<lang>(json);
    }
}


public class lang
{
    public string[] language = new string[12];
}


