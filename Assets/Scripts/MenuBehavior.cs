﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuBehavior : MonoBehaviour {

    public Rigidbody2D rig;
    [SerializeField] float speed = 1.5f;
    [SerializeField] UiController uiC;
    [SerializeField] AudioSource aud;

    void Start ()
    {
        rig = GetComponent<Rigidbody2D>();
    }
	

	void Update ()
    {

        Time.timeScale = 1f;
        if (uiC.effectsActive)
        {
            rig.velocity = new Vector2(0, 0);
            aud.volume = 0;
        }
        else
        {
            aud.volume = 0.13f;
            rig.velocity = new Vector2(speed, rig.velocity.y);
        }

    }
}
