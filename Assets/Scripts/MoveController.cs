﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveController : MonoBehaviour {

    

    public bool grounded;
    public float angleMove;
    GameObject Line;
    [SerializeField] private float radius = 0.4f;

    private void OnDrawGizmos()
    {
        Gizmos.DrawSphere(gameObject.transform.position + new Vector3(0, -0.7f, 0), radius);
    }

    private void FixedUpdate()
    {
        GroundCheck();
    }

    void GroundCheck()
    {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(gameObject.transform.position + new Vector3(0,-0.8f,0), radius);

        grounded =  colliders.Length > 2;

        if (grounded)
        {
            foreach (Collider2D collid in colliders)
            {
                if (collid.tag != "Player")
                {
                    grounded = true;
                    if (collid.gameObject.name == "Collider")
                    {
                        angleMove = Mathf.Cos(collid.gameObject.GetComponentInChildren<Transform>().transform.rotation.ToEuler().z);
                        if (angleMove < 0)
                        {
                            angleMove = 1;
                        }

                    }
                    else
                        angleMove = Mathf.Cos(collid.gameObject.transform.rotation.ToEuler().z);
                }
            }
        }
    }
    
}
