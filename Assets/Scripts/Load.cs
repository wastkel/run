﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.PostProcessing;

public class Load : MonoBehaviour {

    public Image imageLoad;

    void Start ()
    {        
        Invoke("Loading", 3);
	}

    void Loading()
    {
        StartCoroutine(AsyncLoad());
    }

    IEnumerator AsyncLoad()
    {
        AsyncOperation oper = SceneManager.LoadSceneAsync(1);
        while (!oper.isDone)
        {
            float progress = oper.progress / 0.9f;
            imageLoad.fillAmount = progress;
            yield return null;
        }
    }
}
