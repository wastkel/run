﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGen : MonoBehaviour {

    public GameObject pers;
    public GameObject currentLevel;
    public GameObject nextLevel;
    List<GameObject> levelsCounter;// счетчик для удаления старых левелов
    List<GameObject> pullLevels;//пулл левелов для рандома
    List<GameObject> secondPullLevels;//список левелов для ВОССТАНОВЛЕНИЯ pullLevels
    int randomLevel;
    int countLevels;
    [SerializeField] GameObject[] starterLevel;
    [SerializeField] UiController uiController;
    [SerializeField] GameObject [] tutorialLevels;
    [SerializeField] GameObject beginStarter;
    public bool tutorial;
    int countTutorLvl;
    int countMenuLvl;
    GameObject pBeginStarter;

    public void Awake()
    {
        levelsCounter = new List<GameObject>();
        pullLevels = new List<GameObject>();
        secondPullLevels = new List<GameObject>();
        pBeginStarter = Instantiate(beginStarter);
        
        //кол-во левелов !!!!!!!!!!!!!!!!!!!!! Исправить !!!!!!!!!!!!!!!!!!!!!!!!
        while (true)
        {
            if (Resources.Load("LevelPart" + countLevels.ToString()) as GameObject == null)
                break;
            countLevels++;
        }

        //пулл левелов
        for (int i = 0; i < countLevels; i++)
        {
            pullLevels.Add((GameObject)Resources.Load("LevelPart" + i.ToString()) );
        }
        secondPullLevels.AddRange(pullLevels);
        currentLevel = Instantiate(starterLevel[countMenuLvl]);
        countMenuLvl++;
        levelsCounter.Add(currentLevel);
        
    }
	
	void Update ()
    {
        if (uiController.menuActive)
            MenuLevel();
        else if (tutorial)
        {
            TutorialGen();
        }
        else
            CreateLevel();

        pBeginStarter.transform.position = levelsCounter[0].transform.Find("NodeBegin").transform.position +
                pBeginStarter.transform.Find("NodeEnd").transform.localPosition * -1 ;
    }

    

    void TutorialGen()
    {
        
        if (Vector2.Distance(pers.transform.position, currentLevel.transform.Find("NodeEnd").transform.position) < 25f)
        {
            if (countTutorLvl == 2)
            {
                tutorial = false;
                uiController.deathWall.SetActive(true);
            }
            nextLevel = Instantiate(tutorialLevels[countTutorLvl]);
            Trash();
            countTutorLvl++;  
        }
    }

    void Trash()
    {
        nextLevel.transform.position =
                currentLevel.transform.Find("NodeEnd").transform.position +
                nextLevel.transform.Find("NodeBegin").transform.localPosition * -1;
        currentLevel = nextLevel;
        levelsCounter.Add(currentLevel);

        if (levelsCounter.Count > 2)
        {
            Destroy(levelsCounter[0]);
            levelsCounter.RemoveAt(0);
        }
    }

    private void MenuLevel()
    {
        if (Vector2.Distance(pers.transform.position, currentLevel.transform.Find("NodeEnd").transform.position) < 25f)
        {
            nextLevel = Instantiate(starterLevel[countMenuLvl]);
            if (countMenuLvl >= 1)
                countMenuLvl = 0;
            else
                countMenuLvl++;
            Trash();
        }
    }

    private void CreateLevel()
    {
        if (Vector2.Distance(pers.transform.position, currentLevel.transform.Find("NodeEnd").transform.position) < 25f)
        {
            randomLevel = Random.Range(0, pullLevels.Count);
            nextLevel = Instantiate(pullLevels[randomLevel]);
            Trash();
            pullLevels.RemoveAt(randomLevel);
            
        }

        
        if (pullLevels.Count == 0)
        {
            pullLevels.AddRange(secondPullLevels);
        }
    }
}
