﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialEffect : MonoBehaviour {

    [SerializeField] Image obj;
    [SerializeField] SpriteRenderer obj1;
    [SerializeField] float speed;
    float i;

    void Update () {

        if (i >= 0.05)
        {
            if (obj != null)
                obj.color = new Color(obj.color.r, obj.color.g, obj.color.b, i);
            if (obj1 != null)
                obj1.color = new Color(obj1.color.r, obj1.color.g, obj1.color.b, i);
            i -= Time.deltaTime / speed;
        }
        else
            i = 1;
    }
}
