﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapStone : MonoBehaviour {
    // Use this for initialization
    [SerializeField] public float distanceActive;
    [SerializeField] GameObject particle;
    private bool check;

    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Vector3.Distance(gameObject.transform.position, GameObject.FindGameObjectWithTag("Player").transform.position) < distanceActive)
        {
            gameObject.GetComponent<Rigidbody2D>().gravityScale = 3;
            if (!check)
            {
                gameObject.AddComponent<PolygonCollider2D>();
                particle.SetActive(true);
                check = true;
            }
        }
	}

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(gameObject.transform.position, distanceActive);
    }
}
