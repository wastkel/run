﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathWall : MonoBehaviour {
    [SerializeField] UiController ui;
    [SerializeField] GameObject pers;
    float speed = 8;
    // Use this for initialization
    void Start () 
    {
        gameObject.transform.position = pers.transform.position - new Vector3(40, 0, 0);
    }
	
	// Update is called once per frame
	void Update ()
    {
        gameObject.transform.position += new Vector3(1,0,0) * Time.deltaTime * speed;
        gameObject.transform.position = new Vector3(gameObject.transform.position.x, pers.transform.position.y, 0);
        if (Vector2.Distance(pers.transform.position,transform.position) > 50)
        {
            gameObject.transform.position = pers.transform.position - new Vector3(40, 0, 0);
        }
        
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            ui.Restart();
        }
    }
}
