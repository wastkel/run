﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CharController : MonoBehaviour {
    public  Rigidbody2D rig;
    Animator anim;
    MoveController moveCtrl;

    [SerializeField] public float speed = 9f;
    [SerializeField] float impuls = 630;
    [SerializeField] AudioSource audRun;
    [SerializeField] AudioSource audStep;

    private void Awake()
    {
        moveCtrl = GetComponentInChildren<MoveController>();
        rig = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }

    private void FixedUpdate()
    {
        rig.velocity = new Vector2( moveCtrl.angleMove* speed, rig.velocity.y);
    }

    private void Start()
    {
        gameObject.transform.position = GameObject.FindGameObjectWithTag("Respawn").transform.position;
    }

    void Update ()
    {

        if (moveCtrl.grounded && rig.velocity.x > 0.8f )
        {
            audRun.volume = 0.3f;
        }
        else
            audRun.volume = 0;

        if (Input.GetKeyDown(KeyCode.Space) && moveCtrl.grounded)
        {

            rig.AddForce(new Vector2(0f, impuls));
            moveCtrl.grounded = false;
        }
        anim.SetBool("Ground", moveCtrl.grounded);
        anim.SetFloat("vSpeed", rig.velocity.y);
        anim.SetFloat("Speed", rig.velocity.x);
    }

    public void JumpOnButton()
    {
        if (moveCtrl.grounded)
        {
            rig.AddForce(new Vector2(0f, impuls));
            moveCtrl.grounded = false;
        }
    }
}
