﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour {

    UiController ui;

    private void Awake()
    {       
        ui = GameObject.FindGameObjectWithTag("EventSystem").GetComponent<UiController>();
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "Player")
        {
            DrawPhysicsLine drawLine = col.gameObject.GetComponent<DrawPhysicsLine>();
            ui.score += 10;
            if (drawLine.vignette.intensity > 0.11)
            {
                drawLine.vignette.intensity -= 0.1f;
            }
            Destroy(gameObject);
            
        }
    }
}
