﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonController : MonoBehaviour {

	// Use this for initialization
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public void StartGame()
    {
        SceneManager.LoadScene(1);
    }

    public void Test()
    {
        SceneManager.LoadScene(2);
    }

    public void Selected()
    {
        gameObject.GetComponent<UnityEngine.UI.Text>().color = Color.white;
    }

    public void UnSelected()
    {
        gameObject.GetComponent<UnityEngine.UI.Text>().color = Color.grey;
    }
}
