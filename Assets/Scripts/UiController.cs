﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Purchasing;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Advertisements;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;

public class UiController : MonoBehaviour {

    [SerializeField] Text currentMoney;
    [SerializeField] CharController charContr;
    [SerializeField] DrawPhysicsLine drawL;
    [SerializeField] Text scoreUi;
    [SerializeField] Text maxScore;
    [SerializeField] Behaviour[] menu;
    [SerializeField] Behaviour[] game;
    [SerializeField] GameObject canvasMenu;
    [SerializeField] GameObject menuButtons;
    [SerializeField] GameObject canvasGame;
    [SerializeField] GameObject effectsMenu;
    [SerializeField] GameObject mark;
    [SerializeField] public GameObject deathWall;
    [SerializeField] GameObject costTxt;
    [SerializeField] Text costNumber;
    [SerializeField] GameObject menuEffect;
    [SerializeField] Animator animContrl;
    [SerializeField] GameObject buyButton;
    [SerializeField] GameObject acceptButton;
    [SerializeField] GameObject buttonAds;
    [SerializeField] GameObject tutorialMenu;
    [SerializeField] MapGen mapGen;
    [SerializeField] GameObject jump;
    [SerializeField] GameObject jumpLight;
    [SerializeField] GameObject slow;
    [SerializeField] GameObject slowLight;
    [SerializeField] GameObject darkBg;
    [SerializeField] GameObject tutorGameMenu;
    [SerializeField] AudioSource audioMenu;
    [SerializeField] AudioSource audioGame;
    [SerializeField] AudioSource[] sourcesMute;
    [SerializeField] Behaviour[] deathEvent;
    [SerializeField] SpriteRenderer sprt;
    [SerializeField] Rigidbody2D rb;
    [SerializeField] GameObject deathAnim;
    [SerializeField] GameObject soundEIcon;
    [SerializeField] GameObject soundNotIcon;
    [SerializeField] GameObject leaderBoardBtn;
    [SerializeField] GameObject Connect;
    bool death;
    static bool soudEnable = true;
    GameObject pEffect;
    int counter = 0;
    GameObject currentEff;
    const string leaderboard = "CgkI7pG9tPccEAIQAQ";



    public bool menuActive = true;
    public bool effectsActive;
    public float score = 0;
    private static int adsCount = 0;

    [Header("Эффекты")]
    [SerializeField] public GameObject[] effects;
    //Костыль
    [SerializeField] float[] costEffect;

    void Start()
    {
        PlayGamesPlatform.Activate();

        Social.localUser.Authenticate((bool success) =>
        {
            //OnConnectionResponse(success);
        });
        //OnConnectionResponse(PlayGamesPlatform.Instance.localUser.authenticated);

        PurchaseManager.OnPurchaseNonConsumable += PurchaseManager_OnPurchaseNonConsumable;
        PurchaseManager.OnPurchaseConsumable += PurchaseManager_OnPurchaseConsumable;


        if (PlayerPrefs.GetString("ads") != "Buy")
        {
            Advertisement.Initialize("1584505");
            adsCount++;
            if (Advertisement.IsReady() && adsCount >= 6)
            {
                Advertisement.Show();
                adsCount = 1;
            }
        }
        else
            buttonAds.SetActive(false);

        PlayerPrefs.SetString("0", "Buy");
        MenuEffects();
        FirstSave();
        SwapPlayMode();
        Tutorial();
    }

    public void OnConnectClick()
    {
        Social.localUser.Authenticate((bool success) =>
        {
            //OnConnectionResponse(success);
        });
    }

    //private void OnConnectionResponse(bool authenticated)
    //{
    //    if(authenticated)
    //    {
    //        leaderBoardBtn.SetActive(true);
    //    }
    //    else
    //    {
    //        leaderBoardBtn.SetActive(false);
    //    }
    //}

    public void ShowAd()
    {
        if (Advertisement.IsReady())
        {
            Advertisement.Show("rewardedVideo", new ShowOptions() { resultCallback = HandleAdResult });
        }
    }

    private void HandleAdResult(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                PlayerPrefs.SetFloat("Money", PlayerPrefs.GetFloat("Money") + 300f);
                break;
            case ShowResult.Skipped:
                break;
            case ShowResult.Failed:
                break;
        }
    }

    private void PurchaseManager_OnPurchaseConsumable(PurchaseEventArgs args)
    {
        PlayerPrefs.SetFloat("Money", PlayerPrefs.GetFloat("Money") + 25000);
    }

    private void PurchaseManager_OnPurchaseNonConsumable(PurchaseEventArgs args)
    {
        BuyAds();
    }

    void Update()
    {
        //TODO : paticle effectsBuy
        /*m_Particles = new ParticleSystem.Particle[coinsEffect.maxParticles];
        int numParticlesAlive = coinsEffect.GetParticles(m_Particles);
        for (int i = 0; i < numParticlesAlive; i++)
        {
            //m_Particles[i].velocity += Vector3.up * 0.1f;

            m_Particles[i].position = Vector3.SlerpUnclamped(m_Particles[i].position, mark.transform.position, 1); //Vector3.Lerp(m_Particles[i].position, new Vector3(0,-10,0),100);
            coinsEffect.SetParticles(m_Particles, numParticlesAlive);
        }*/
        SoundEnable();

        if (!menuActive && !mapGen.tutorial && !death)
        {
            score += Time.deltaTime;
            scoreUi.text = ((int)score).ToString();
            audioGame.volume += Time.deltaTime * 2;
            if (audioMenu.volume >= 0.05f)
                audioMenu.volume -= Time.deltaTime / 2;
        }
        else
        {
            audioMenu.volume += Time.deltaTime * 2;
            if (audioGame.volume >= 0.05f)
                audioGame.volume -= Time.deltaTime / 2;
            if (!mapGen.tutorial)
            {
                pEffect.transform.position = Vector3.Lerp(pEffect.transform.position, new Vector3(menuEffect.transform.position.x, menuEffect.transform.position.y, -5), 0.5f);
                if (PlayerPrefs.GetFloat("Money") < float.Parse(currentMoney.text))
                {

                    currentMoney.text = (float.Parse(currentMoney.text) - costEffect[counter] / 50).ToString();
                }
                else
                    currentMoney.text = PlayerPrefs.GetFloat("Money").ToString();
                BuyUI();
            }
        }
    }

    public void ButtonYes()
    {
        PlayerPrefs.SetString("tutorial", "1");
        mapGen.tutorial = true;
        tutorGameMenu.SetActive(true);
        StartGame();
        charContr.speed = 0;
    }

    public void ButtonOk()
    {
        tutorGameMenu.SetActive(false);
        charContr.speed = 9f;
    }

    public void ButtonNo()
    {
        PlayerPrefs.SetString("tutorial", "1");
    }

    void Tutorial()
    {
        if (PlayerPrefs.GetString("tutorial") == "")
        {
            tutorialMenu.SetActive(true);
            canvasMenu.SetActive(false);
        }
        else
        {
            canvasMenu.SetActive(true);
            tutorialMenu.SetActive(false);
        }
    }

    public void BuyAds()
    {
        PlayerPrefs.SetString("ads", "Buy");
    }


    public void Restart()
    {
        Save();

        Time.timeScale = 1;
        drawL.vignette.intensity = 0.2f;

        

        SceneManager.LoadScene(1);

    }

    private void FirstSave()
    {
        if (PlayerPrefs.GetString("MaxScore") == "")
        {
            PlayerPrefs.SetString("MaxScore", "0");
            maxScore.text = "0";
        }
        else
            maxScore.text = PlayerPrefs.GetString("MaxScore");
    }

    private void Save()
    {
        /// record
        if (PlayerPrefs.GetString("MaxScore") != "")
        {
            if (float.Parse(PlayerPrefs.GetString("MaxScore")) < float.Parse(scoreUi.text))
            {
                PlayerPrefs.SetString("MaxScore", scoreUi.text);
                Social.ReportScore(long.Parse(scoreUi.text), leaderboard, (bool success) => { });
            }
        }

        /// current Money
        PlayerPrefs.SetFloat("Money", PlayerPrefs.GetFloat("Money") + float.Parse(scoreUi.text));
    }

    public void LeaderBtn()
    {
        if (Social.localUser.authenticated)
            Social.ShowLeaderboardUI();
    }

    public void SwapPlayMode()
    {
        if (menuActive)
        {
            foreach (Behaviour i in game)
                i.enabled = false;
            foreach (Behaviour i in menu)
                i.enabled = true;
            canvasMenu.SetActive(true);
            canvasGame.SetActive(false);
            deathWall.SetActive(false);
        }
        else
        {
            foreach (Behaviour i in game)
                i.enabled = true;
            foreach (Behaviour i in menu)
                i.enabled = false;
            canvasGame.SetActive(true);
            canvasMenu.SetActive(false);
            if (mapGen.tutorial != true)
                deathWall.SetActive(true);
        }
    }

    public void StartGame()
    {
        menuActive = false;
        SwapPlayMode();
        drawL.AcceptEffect();
        Destroy(pEffect);
        animContrl.SetBool("Play", true);
    }

    public void Test()
    {
        SceneManager.LoadScene(2);
    }

    public void SwapEffects()
    {
        effectsActive = true;
        currentEff = Instantiate(effects[PlayerPrefs.GetInt("ParticleLine")], new Vector3(mark.transform.position.x, mark.transform.position.y, -5), transform.rotation);
        menuButtons.SetActive(false);
        effectsMenu.SetActive(true);
        pEffect.SetActive(false);
        animContrl.SetBool("CastEff", true);
        counter = PlayerPrefs.GetInt("ParticleLine");
        //BuyUI();
    }

    public void Next()
    {
        Destroy(currentEff);
        counter++;
        if (counter > effects.Length - 1)
            counter = 0;
        currentEff = Instantiate(effects[counter], new Vector3(mark.transform.position.x, mark.transform.position.y, -5), transform.rotation);
        animContrl.Play("Cast", 0);
        //BuyUI();
    }

    public void Back()
    {
        Destroy(currentEff);
        counter--;
        if (counter < 0)
            counter = effects.Length - 1;
        currentEff = Instantiate(effects[counter], new Vector3(mark.transform.position.x, mark.transform.position.y, -5), transform.rotation);
        animContrl.Play("Cast", 0);
        //BuyUI();
    }

    void MenuEffects()
    {
        pEffect = Instantiate(effects[PlayerPrefs.GetInt("ParticleLine")], menuEffect.transform.position, menuEffect.transform.rotation);
        //pEffect.transform.parent = menuEffect.transform;
        ParticleSystem.ShapeModule shapeModule = pEffect.GetComponent<ParticleSystem>().shape;
        foreach (ParticleSystem i in pEffect.GetComponentsInChildren<ParticleSystem>())
        {
            shapeModule = i.shape;
            shapeModule.shapeType = ParticleSystemShapeType.Sphere;
            shapeModule.radius = 1.25f;
        }
    }

    public void Accept()
    {
        PlayerPrefs.SetInt("ParticleLine", counter);
        effectsActive = false;
        Destroy(currentEff);
        Destroy(pEffect);
        MenuEffects();
        menuButtons.SetActive(true);
        effectsMenu.SetActive(false);
        pEffect.SetActive(true);
        animContrl.SetBool("CastEff", false);
    }

    public void BackMenu()
    {
        effectsActive = false;
        Destroy(currentEff);
        menuButtons.SetActive(true);
        effectsMenu.SetActive(false);
        pEffect.SetActive(true);
        animContrl.SetBool("CastEff", false);
    }

    void BuyUI()
    {
        if (PlayerPrefs.GetString(counter.ToString()) == "")
        {
            buyButton.SetActive(true);
            acceptButton.SetActive(false);
            costTxt.SetActive(true);
            costNumber.text = costEffect[counter].ToString();
        }
        else
        {
            costTxt.SetActive(false);
            buyButton.SetActive(false);
            acceptButton.SetActive(true);
        }
    }

    public void BuyEffect()
    {
        if (PlayerPrefs.GetFloat("Money") >= costEffect[counter])
        {
            PlayerPrefs.SetFloat("Money", PlayerPrefs.GetFloat("Money") - costEffect[counter]);
            PlayerPrefs.SetString(counter.ToString(), "Buy");
        }
    }

    public void ResetAchiev()
    {
        for (int i = 0; i <= effects.Length; i++)
        {
            PlayerPrefs.DeleteKey(i.ToString());
        }
        PlayerPrefs.SetFloat("Money", PlayerPrefs.GetFloat("Money") + 1000);
        PlayerPrefs.SetInt("ParticleLine", 0);
        PlayerPrefs.SetString("0", "Buy");
        PlayerPrefs.DeleteKey("tutorial");
        PlayerPrefs.DeleteKey("ads");
    }

    public void SoundChange()
    {
        soudEnable = !soudEnable;
    }

    public void SoundEnable()
    {
        if (soudEnable)
        {
            soundEIcon.SetActive(true);
            soundNotIcon.SetActive(false);
            foreach (AudioSource s in sourcesMute)
                s.mute = false;
        }
        else
        {
            soundEIcon.SetActive(false);
            soundNotIcon.SetActive(true);
            foreach (AudioSource s in sourcesMute)
                s.mute = true;
        }
    }

    public void DeathEvent()
    {
        death = true;
        sprt.enabled = false;
        rb.bodyType = RigidbodyType2D.Static;
        deathAnim.SetActive(true);
        foreach (Behaviour i in deathEvent)
        {
            i.enabled = false;
        }
        Invoke("Restart",3f);
    }
}
