﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.PostProcessing;

public class DrawPhysicsLine : MonoBehaviour
{
    LineRenderer line; // Reference to LineRenderer
    Vector3 mousePos;
    Vector3 startPos;    // Start position of line
    Vector3 endPos;    // End position of line
    


    bool slow;
    [SerializeField] GameObject particle;
    [SerializeField] UiController uiContrl;

    Color color;
    RaycastHit2D hit;
    GameObject objLine;
    GameObject oldLine;
    GameObject prefP;

    public GameObject colliderObj;
    public float angle;
    public BoxCollider2D col;

    [SerializeField] PostProcessingProfile postProcProf;
    public VignetteModel.Settings vignette; 

    void Update()
    {
        SlowChange();
        hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector3.zero);

        if (hit.collider == null)
        {
            DrawLine();
        }
    }

    private void SlowChange()
    {
        if (slow)
        {
            Time.timeScale = 0.3f;
            vignette.intensity += Time.deltaTime * 0.2f;
            postProcProf.vignette.settings = vignette;
            if (vignette.intensity >= 0.6f)
                slow = !slow;
        }
        else
        {
            Time.timeScale = 1;
            if (vignette.intensity >= 0.2f)
            {
                vignette.intensity -= Time.deltaTime * 0.3f;
                postProcProf.vignette.settings = vignette;
            }
        }
    }

    private void DrawLine()
    {
        hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector3.zero);

            if (Input.GetMouseButtonDown(0))
            {
                createLine();
                mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                mousePos.z = 0;
                line.SetPosition(0, mousePos);
                line.SetWidth(0, 0);
                startPos = mousePos;
            }
            else if (Input.GetMouseButtonUp(0))
            {
                if (line)
                {
                    mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    mousePos.z = 0;
                    line.SetPosition(1, mousePos);
                    endPos = mousePos;
                    hit = Physics2D.Raycast(startPos, endPos - startPos, Vector3.Distance(startPos, endPos));
                    if (hit.collider != null && (hit.collider.tag == "Player" || hit.collider.tag == "Spike" || hit.collider.tag == "Stone"))
                    {
                        Destroy(objLine);
                    }
                    else
                    {
                        Destroy(oldLine);
                        addColliderToLine();
                        color.a = 0;
                        line.SetColors(color, color);
                        oldLine = objLine;
                    }
                }
            }
            else if (Input.GetMouseButton(0))
            {
                if (line)
                {
                    mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    mousePos.z = 0;
                    line.SetPosition(1, mousePos);
                    line.SetWidth(0.2f, 0.2f);
                }
            }
    }

    /*
#region Button Effects
public void Enter()
{
   check = true;
}

public void Exit()
{
   check = false;
}
#endregion
*/
    public void Slow()
    {
        slow = !slow;
        
    }

    // Following method creates line runtime using Line Renderer component
    private void createLine()
    {

        objLine = new GameObject("Line");
        line = objLine.AddComponent<LineRenderer>();
        line.material = Resources.Load("ColorLine") as Material;
        color = Color.gray;
        color.a = 0.5f;
        line.SetColors(color, color);
        line.useWorldSpace = true;
    }
    // Following method adds collider to created line
    private void addColliderToLine()
    {
        colliderObj = new GameObject("Collider");
        col = colliderObj.AddComponent<BoxCollider2D>();
        col.transform.parent = line.transform; // Collider is added as child object of line
        float lineLength = Vector3.Distance(startPos, endPos); // length of line

        #region Material & Layer

        col.sharedMaterial = new PhysicsMaterial2D("MaterialLine");
        col.sharedMaterial.bounciness = 0;
        col.sharedMaterial.friction = 0;
        colliderObj.layer = 8;

        #endregion
        
        #region ColliderParam

        col.size = new Vector3(lineLength, 0.2f, 1f); // size of collider is set where X is length of line, Y is width of line, Z will be set as per requirement
        Vector3 midPoint = (startPos + endPos) / 2;
        col.transform.position = midPoint; // setting position of collider object
        // Following lines calculate the angle between startPos and endPos
        angle = 0;
        angle = (Mathf.Abs(startPos.y - endPos.y) / Mathf.Abs(startPos.x - endPos.x));
        if ((startPos.y < endPos.y && startPos.x > endPos.x) || (endPos.y < startPos.y && endPos.x > startPos.x))
        {
            angle *= -1;
        }
        angle = Mathf.Rad2Deg * Mathf.Atan(angle);
        col.transform.Rotate(0, 0, angle);

        #endregion

        #region Particle

        prefP = Instantiate(particle, objLine.transform);
        
        ParticleSystem.ShapeModule shapeModule = prefP.GetComponent<ParticleSystem>().shape;
        shapeModule.radius = lineLength / 2;
        foreach (ParticleSystem i in prefP.GetComponentsInChildren<ParticleSystem>())
        {
            shapeModule = i.shape;
            /////////////// !!!! ALERT !!! ////////////////////////
            if (i.shape.radius > 3) //// KASTYL'
                shapeModule.radius = lineLength / 2;
        }
        prefP.transform.position = midPoint;
        prefP.transform.Rotate(0, 0, angle);
        #endregion
    }

    public void AcceptEffect()
    {
        particle = uiContrl.effects[PlayerPrefs.GetInt("ParticleLine")];
    }

    private void Start()
    {
        vignette = postProcProf.vignette.settings;
        vignette.intensity = 0.2f;
        postProcProf.vignette.settings = vignette;
        //ppp.vignette.settings.intensity = 0.18f;
    }
}