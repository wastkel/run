﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TestEffects : MonoBehaviour {

    [SerializeField] GameObject[] effects;
    int counter = 0;
    GameObject currentEff;



    void Start ()
    {
        currentEff = Instantiate(effects[counter], new Vector3(0, 0, 0), transform.rotation);
	}

    public void Next()
    {
        Destroy(currentEff);
        counter++;
        if (counter > effects.Length - 1)
            counter = 0;
        currentEff = Instantiate(effects[counter], new Vector3(0, 0, 0), transform.rotation);
    }

    public void Back()
    {
        Destroy(currentEff);
        counter--;
        if (counter < 0)
            counter = effects.Length - 1;
        currentEff = Instantiate(effects[counter], new Vector3(0, 0, 0), transform.rotation);
    }

    public void LoadMenu()
    {
        SceneManager.LoadScene(1);
    }

    public void Accept()
    {
        PlayerPrefs.SetInt("ParticleLine", counter);
    }
}
