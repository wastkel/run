﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Trap : MonoBehaviour {

    UiController ui;
    int maxScore;
    

    private void Awake()
    {

    }

    void Start ()
    {
        ui = GameObject.FindGameObjectWithTag("EventSystem").GetComponent<UiController>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        
    }
    
    private void OnCollisionEnter2D(Collision2D col)
    {
        
        if (col.gameObject.tag == "Player")
        {
            ui.DeathEvent();
        }
        
    }
}
