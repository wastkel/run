﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Menu : MonoBehaviour {
    Animator anim;
	// Use this for initialization
	void Start ()
    {
        anim = gameObject.GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
        if ((Input.GetMouseButton(0)))
        {
            anim.enabled = false;
            var mpos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            gameObject.transform.position = Vector3.Slerp(gameObject.transform.position, new Vector3(mpos.x, mpos.y, 0), 1);
        }
        if ((Input.GetMouseButtonUp(0)))
        {
            anim.enabled = true;
        }

    }
}
